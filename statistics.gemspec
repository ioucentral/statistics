# -*- encoding: utf-8 -*-
# stub: statistics 1.0.4 ruby lib

Gem::Specification.new do |s|
  s.name = "statistics".freeze
  s.version = "1.0.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Alexandru Catighera".freeze]
  s.date = "2010-09-10"
  s.email = "acatighera@gmail.com".freeze
  s.extra_rdoc_files = ["README.markdown".freeze]
  s.files = [".gitignore".freeze, "CHANGES.markdown".freeze, "MIT-LICENSE".freeze, "README.markdown".freeze, "Rakefile".freeze, "VERSION".freeze, "init.rb".freeze, "lib/statistics.rb".freeze, "statistics.gemspec".freeze, "test/statistics_test.rb".freeze]
  s.homepage = "http://github.com/tam-vo/statistics".freeze
  s.rdoc_options = ["--charset=UTF-8".freeze]
  s.rubygems_version = "2.7.6".freeze
  s.summary = "An ActiveRecord gem that makes it easier to do reporting.".freeze
  s.test_files = ["test/statistics_test.rb".freeze]

  s.installed_by_version = "2.7.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<retriable>.freeze, [">= 0"])
    else
      s.add_dependency(%q<retriable>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<retriable>.freeze, [">= 0"])
  end
end
