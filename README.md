# IOU FINANCIAL HOSTED STATISTICS

This is a copy of a fork of the `statistics` gem found [here](https://github.com/acatighera/statistics).  The fork used previously was hosted at
`https://github.com/tam-vo/statistics.git`

That repo no longer exists. The files here are to the best of our knowledge, the files that existed in the `tam-vo` fork.

There is no claim to the functionality of this gem and the gem is hosted AS-IS.